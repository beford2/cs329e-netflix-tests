#!/usr/bin/env python3

# -------
# imports
# -------
from Netflix import netflix_eval, netflix_rmse, netflix_prediction
from unittest import main, TestCase
from math import sqrt
from io import StringIO
from numpy import sqrt, square, mean, subtract

# -----------
# TestNetflix
# -----------


class TestNetflix (TestCase):

    # ----
    # eval
    # ----

    # Test with same order of customerID in probe.txt
    def test_netflix_eval_1(self):
        r = StringIO("10040:\n2417853\n1207062\n2487973\n")
        w = StringIO()
        netflix_eval(r, w)
        w_contents = w.getvalue()
        mID = w_contents.split(':')[0]
        self.assertEqual(mID, "10040")

    # Test with same customerIDs
    def test_netflix_eval_2(self):
        r = StringIO("1:\n30878\n30878\n30878\n")
        w = StringIO()
        netflix_eval(r, w)
        w_contents = w.getvalue()
        mID = w_contents.split(':')[0]
        self.assertEqual(mID, "1")

    # Test with different order of customerID as compared to probe.txt
    def test_netflix_eval_3(self):
        r = StringIO("1000:\n2326571\n1010534\n977808\n")
        w = StringIO()
        netflix_eval(r, w)
        w_contents = w.getvalue()
        mID = w_contents.split(':')[0]
        self.assertEqual(mID, "1000")

    # picked one person from probe.txt and check that prediction is between 1 an 5
    def test_netflix_prediction_1(self):
        assert netflix_prediction(10007, 1204847) >= 1.00 and netflix_prediction(
            10007, 1204847) <= 5.00

    # picked one person from probe.txt and check that prediction is between 1 and 5
    def test_netflix_prediction_2(self):
        assert netflix_prediction(
            1000, 2326571) >= 1.00 and netflix_prediction(1000, 2326571) <= 5.00

    # picked one person from probe.txt and check that prediction is between 1 and 5
    def test_netflix_prediction_3(self):
        assert netflix_prediction(10040, 2417853) >= 1.00 and netflix_prediction(
            10040, 2417853) <= 5.00

    # test predictions and actual at the ends of the rating spectrum
    def test_RMSE_1(self):
        predictions = [1.0, 4.99, 1.11, 5.0]
        actual = [2, 5, 1, 5]
        assert netflix_rmse(predictions, actual) < 1.00

    # test rmse if predictions and actual are the exact same
    def test_RMSE_2(self):
        predictions = [1.0, 2.0, 1.11, 4.98]
        actual = [1.0, 2.0, 1.11, 4.98]
        assert netflix_rmse(predictions, actual) < 1.00

    # test rmse if predictions have multiple numbers after decimal place even if they are the same value
    def test_RMSE_3(self):
        predictions = [5.0, 5.00, 5.000, 5.000]
        actual = [4, 5, 4, 5]
        assert netflix_rmse(predictions, actual) < 1.00


# ----
# main
# ----
if __name__ == '__main__':
    main()

""" #pragma: no cover
% coverage3 run --branch TestNetflix.py >  TestNetflix.out 2>&1



% coverage3 report -m                   >> TestNetflix.out



% cat TestNetflix.out
.
----------------------------------------------------------------------
Ran 1 test in 0.000s

OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Netflix.py          27      0      4      0   100%
TestNetflix.py      13      0      0      0   100%
------------------------------------------------------------
TOTAL               40      0      4      0   100%

"""
